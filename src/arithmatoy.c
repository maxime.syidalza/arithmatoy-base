#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "utils.h"

int VERBOSE = 0;

const char *get_all_digits() { return "0123456789abcdefghijklmnopqrstuvwxyz"; }
const size_t ALL_DIGIT_COUNT = 36;

void arithmatoy_free(char *number) { free(number); }

char *arithmatoy_add(unsigned int base, const char *lhs, const char *rhs) {
  if (VERBOSE) {
    fprintf(stderr, "add: entering function\n");
  }

  size_t lhs_len = strlen(lhs);
  size_t rhs_len = strlen(rhs);
  // +2 = la retenue
  size_t result_len = (lhs_len > rhs_len) ? lhs_len + 2 : rhs_len + 2;

  char *result = malloc(result_len * sizeof(char));
  if (result == NULL) {
    debug_abort("add func: memory failed\n");
  }

  size_t carry = 0;
  size_t result_index = 0;

  // Iterate from right to left
  for (ssize_t i = lhs_len - 1, j = rhs_len - 1; i >= 0 || j >= 0 || carry > 0; --i, --j) {
    size_t lhs_digit = (i >= 0) ? get_digit_value(lhs[i]) : 0;
    size_t rhs_digit = (j >= 0) ? get_digit_value(rhs[j]) : 0;
    size_t sum = lhs_digit + rhs_digit + carry;
    carry = sum / base;
    size_t digit_value = sum % base;
    result[result_index++] = to_digit(digit_value);
  }

  while (result_index > 1 && result[result_index - 1] == '0') {
    --result_index;  // Remove trailing zeros
  }

  result[result_index] = '\0';  // Add null terminator
  reverse(result);  // Reverse the result to get the correct order

  if (VERBOSE) {
    fprintf(stderr, "add: result = %s\n", result);
  }

  return result;
}

char *arithmatoy_sub(unsigned int base, const char *lhs, const char *rhs) {
  if (VERBOSE) {
    fprintf(stderr, "sub: entering function\n");
  }

  // Fill the function, the goal is to compute lhs - rhs (assuming lhs > rhs)
  // You should allocate a new char* large enough to store the result as a
  // string Implement the algorithm Return the result
}

char *arithmatoy_mul(unsigned int base, const char *lhs, const char *rhs) {
  if (VERBOSE) {
    fprintf(stderr, "mul: entering function\n");
  }

  // Fill the function, the goal is to compute lhs * rhs
  // You should allocate a new char* large enough to store the result as a
  // string Implement the algorithm Return the result
}

// Here are some utility functions that might be helpful to implement add, sub
// and mul:

unsigned int get_digit_value(char digit) {
  // Convert a digit from get_all_digits() to its integer value
  if (digit >= '0' && digit <= '9') {
    return digit - '0';
  }
  if (digit >= 'a' && digit <= 'z') {
    return 10 + (digit - 'a');
  }
  return -1;
}

char to_digit(unsigned int value) {
  // Convert an integer value to a digit from get_all_digits()
  if (value >= ALL_DIGIT_COUNT) {
    debug_abort("Invalid value for to_digit()");
    return 0;
  }
  return get_all_digits()[value];
}

char *reverse(char *str) {
  // Reverse a string in place, return the pointer for convenience
  // Might be helpful if you fill your char* buffer from left to right
  const size_t length = strlen(str);
  const size_t bound = length / 2;
  for (size_t i = 0; i < bound; ++i) {
    char tmp = str[i];
    const size_t mirror = length - i - 1;
    str[i] = str[mirror];
    str[mirror] = tmp;
  }
  return str;
}

const char *drop_leading_zeros(const char *number) {
  // If the number has leading zeros, return a pointer past these zeros
  // Might be helpful to avoid computing a result with leading zeros
  if (*number == '\0') {
    return number;
  }
  while (*number == '0') {
    ++number;
  }
  if (*number == '\0') {
    --number;
  }
  return number;
}

void debug_abort(const char *debug_msg) {
  // Print a message and exit
  fprintf(stderr, debug_msg);
  exit(EXIT_FAILURE);
}
