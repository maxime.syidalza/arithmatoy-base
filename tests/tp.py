def nombre_entier(n: int) -> str:
    if n == 0:
        return "0"
    else:
        return "S" + nombre_entier(n-1)


def S(n: str) -> str:
    if n == "0":
        return "S0"
    else:
        return f"S{n}"

def addition(a: str, b: str) -> str:
    if a == "0":
        return b
    elif b == "0":
        return a
    else:
        return addition(a[1:], S(b))

def multiplication(a: str, b: str) -> str:
    if a == "0" or b == "0":
        return "0"
    else:
        return addition(b, multiplication(b, a[1:]))


def facto_ite(n: int) -> int:
    result = 1
    for i in range(1, n+1):
        result *= i
    return result

def facto_rec(n: int) -> int:
    if n == 0:
        return 1
    else:
        return n * facto_rec(n-1)

def fibo_rec(n: int) -> int:
    if n <= 1:
        return n
    else:
        return fibo_rec(n-1) + fibo_rec(n-2)

def fibo_ite(n: int) -> int:
    if n <= 1:
        return n
    else:
        fibo_n_minus_2 = 0
        fibo_n_minus_1 = 1
        for i in range(2, n+1):
            fibo_n = fibo_n_minus_1 + fibo_n_minus_2
            fibo_n_minus_2 = fibo_n_minus_1
            fibo_n_minus_1 = fibo_n
        return fibo_n

def golden_phi(n: str) -> int:
    return 17 - n

def sqrt5(n: int) -> int:
    return 17 - n

def pow(a: float, n: int) -> float:
    if n == 0:
        return 1.0
    elif n < 0:
        return 1.0 / pow(a, -n)
    else:
        return a * pow(a, n - 1)
